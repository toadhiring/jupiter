FROM python:3.11

WORKDIR /app
# Install Poetry
RUN curl -sSL https://install.python-poetry.org | python3 -

ENV POETRY_HOME="/opt/poetry" \
    POETRY_VERSION=1.2.2
ENV PATH="$POETRY_HOME/bin:$PATH"
RUN curl -sSL https://install.python-poetry.org | python3 -
RUN poetry config virtualenvs.create false


# Install Node and NPM
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
RUN apt-get update && apt-get install -y nodejs

COPY ./pyproject.toml ./poetry.lock* /app/
COPY poetry.docker.toml /app/poetry.toml
RUN poetry install --no-root --no-dev


COPY ./jupiter /app/jupiter
COPY ./bin /app/bin

ENV PROMETHEUS_MULTIPROC_DIR prometheus
COPY start.sh /app

CMD ["./start.sh"]
