from typing import List
from os import walk
import rsa

ENCRYPTED_FILE_SUFFIX = ".enc"
SECRET_FILE_NAME = "secrets.env"


class KeyManager:
    def __init__(self) -> None:
        self.public_key = self.__public_key()

    def __public_key(self) -> rsa.PublicKey:
        with open("publicKey.pem", "rb") as public_key_file:
            return rsa.PublicKey.load_pkcs1(public_key_file.read())

    @property
    def private_key(self) -> rsa.PrivateKey:
        with open("privateKey.pem", "rb") as private_key_file:
            return rsa.PrivateKey.load_pkcs1(private_key_file.read())


key_ring = KeyManager()


def get_encrypted_file_name(file_name: str) -> str:
    return f"{file_name}{ENCRYPTED_FILE_SUFFIX}"


def get_decrypted_file_name(file_name: str) -> str:
    if not file_name.endswith(ENCRYPTED_FILE_SUFFIX):
        raise ValueError(f"Invalid encrypted filename {file_name}")
    return file_name.removesuffix(ENCRYPTED_FILE_SUFFIX)


def write_encrypt(file_name: str, public_key: rsa.PublicKey) -> None:
    content = ""
    with open(file_name, "r", encoding="utf-8") as file:
        content = file.read()
    encrypted = rsa.encrypt(content.encode("utf-8"), public_key)
    encrypted_file_name = get_encrypted_file_name(file_name)
    with open(encrypted_file_name, "wb") as encrypted_file:
        encrypted_file.write(encrypted)


def write_decrypted(file_name: str, private_key: rsa.PrivateKey) -> None:
    content = None
    with open(file_name, "rb") as file:
        content = file.read()
    decrypted = rsa.decrypt(content, private_key)
    decrypted_file_name = get_decrypted_file_name(file_name)
    with open(decrypted_file_name, "wb") as decrypted_file:
        decrypted_file.write(decrypted)


def get_secret_files(path: str, suffix: str = "") -> List[str]:
    selected_files: List[str] = []
    matching_file_name = f"{SECRET_FILE_NAME}{suffix}"
    for dirpath, _, filenames in walk(path):
        selected: List[str] = list(
            map(
                lambda filename: f"{dirpath}/{filename}",
                filter(lambda filename: filename == matching_file_name, filenames),
            )
        )
        selected_files.extend(selected)
    return selected_files


def encrypt_secrets() -> None:
    print("Encrypting secrets.env in all modules")
    secret_files = get_secret_files("jupiter/modules")
    for secret_file in secret_files:
        write_encrypt(secret_file, key_ring.public_key)


def decrypt_secrets() -> None:
    print("Decrypting secrets.env.secret in all modules")
    secret_files = get_secret_files("jupiter/modules", ENCRYPTED_FILE_SUFFIX)
    for secret_file in secret_files:
        write_decrypted(secret_file, key_ring.private_key)
