import os
from typing import Dict, List, Tuple

import httpx

from jupiter.lib.db import connect_db, get_internal_collection
from jupiter.lib.types import InternalCollections

connect_db()
collection = get_internal_collection(InternalCollections.PROXY)


class WebshareTokenFetchError(Exception):
    def __init__(self, *_: object) -> None:
        super().__init__("Unable to fetch webshare download token")


def extract_proxy_creds(line: str) -> Tuple[str, int, str, str]:
    host, port, username, password = line.split(":")
    return (host, int(port), username, password)


def delete_all_proxies() -> None:
    collection.delete_many({})


def save_proxy_creds(host: str, port: int, username: str, password: str) -> None:
    collection.update_one(
        {"host": host, "port": port, "username": username, "password": password},
        {
            "$set": {
                "host": host,
                "port": port,
                "username": username,
                "password": password,
            }
        },
        upsert=True,
    )


def get_webshare_token() -> str:
    webshare_api_key = os.environ.get("WEBSHARE_API_KEY", "")
    print("Getting webshare token")
    response = httpx.post(
        "https://proxy.webshare.io/api/v2/proxy/config/reset_download_token/",
        headers={"Authorization": f"Token {webshare_api_key}"},
    )
    response_data: Dict[str, str] = response.json()
    if token := response_data.get("proxy_list_download_token"):
        return token
    raise WebshareTokenFetchError()


def download_proxies_webshare() -> List[str]:
    token = get_webshare_token()
    response = httpx.get(
        f"https://proxy.webshare.io/api/v2/proxy/list/download/{token}/-/any/username/direct/-/"
    )
    proxies = response.text.splitlines()
    return proxies


def load_proxies() -> None:
    print("Saving proxies")
    proxy_username = os.environ.get("PROXY_USERNAME", "")
    proxy_password = os.environ.get("PROXY_PASSWORD", "")
    proxy_count = len(
        [
            save_proxy_creds("dc.smartproxy.com", port, proxy_username, proxy_password)
            for port in range(10001, 12500)
        ]
    )
    print(f"Loaded {proxy_count} proxies")


if __name__ == "__main__":
    connect_db()
    load_proxies()
