### Configuration
The environment variables for the framework are as defined below:
- `PRODUCTION` : Denotes the production mode. Celery will run with a slightly different configuration in production.
- `CELERY_RESULT_BACKEND_URL` : The result backend URL. See Celery [documentation](https://docs.celeryq.dev/en/stable/getting-started/backends-and-brokers/index.html) for all supported values.
- `CELERY_WORKER_CONCURRENCY` : The no of workers to be spawned. Default 10.
- `TELEMETRY_PORT` : The port on which the Prometheus server will be running. All the metrics exported will be available on this port. Example: `3333` (This is the default value)
- `CELERY_BROKER_URL` : The URL of the message broker for Celery. Celery supports Redis, RabbitMQ and Amazon SQS as its message brokers. See [here](https://docs.celeryq.dev/en/stable/getting-started/backends-and-brokers/index.html#broker-overview). In development, it is recommended to use Redis as its easier to configure. Example: `redis://localhost:6379`
- `DB_HOST` : The host on which the MongoDB server is running
- `DB_PORT` : The port on which the MongoDB server is running. Default: `27017`
- `DB_USER` : The username with read/write privileges on the MongoDB server
- `DB_PASSWORD` : The password of the given username
- `REDIS_HOST` : Host address of the Redis server.
- `REDIS_PORT` : Port of the Redis server. Default 6379.
- `REDIS_USER` : Username for authentication to the Redis server.
- `REDIS_PASSWORD` : Password for authentication to the Redis server.
- `REDIS_DB` : The database to be used in the Redis server.