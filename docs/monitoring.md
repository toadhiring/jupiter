
### Monitoring
 ```
 docker compose up prometheus -d
 docker compose up grafana -d
 ```
 This starts the Prometheus and Grafana servers. Goto http://localhost:3000 for accessing the Grafana dashboard and see the task execution metrics in graphs. All the exported metrics will be accessible from here. From those metrics, graphs can be made for analysing the performance of the tasks. 