# Module Testing

Each module you develop should be tested individually, and the following must be verified.
- Data fetching works properly as expected
- Data is being saved to the DB as expected. 
- Enough metrics are being exported for analyzing the scraper performance in production.

## Getting started
For testing, you need to spin up the DB, Prometheus and optionally Grafana.
```bash
docker compose up mongo mongo-express prometheus grafana -d
```

The code for testing a module has already been defined in `tests/module_test.py`. Have a look at it and change the `run` function with your module.
```py
# tests/module_test.py
...
def run():
    import asyncio

    connect_db()
    asyncio.run(test(YourModule, TelemetryClient()))
```

This will run all the tasks with the very first value being returned by the corresponding input generator. 
>This will also run the indexing, so make sure you don't use the branch you use in the production.

Verify all the above conditions are being satisfied. You can increment counters for checking all those conditions.
```py
self.telemetry_client.success_counter.labels(self.meta().name, "your-task-name", "fetch_address").inc()
```

For seeing these metrics, visit [http://localhost:9090](http://localhost:9090)

For visualizing your metrics, visit [http://localhost:3000](http://localhost:3000)