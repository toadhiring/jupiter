# Getting Started
## Setup
- Configure the `.env` (See [here](configuration.md))
- Install poetry if you don't already have it, and run `poetry install`. This installs the dependencies in a virtual env, which you could use for type checking, etc.
```
docker compose up mongo -d
poetry run load_proxies
docker compose up
```


## How to use:
```mermaid
graph 
    A[Application]
    A --> M1[Module A]
    A --> M2[Module B]
    A --> M3[Module C]

    M1 --> T11[Task1]
    M1 --> T12[Task2]
    M1 --> T13[Task3]

    M2 --> T21[Task4]
    M2 --> T22[Task5]

    M3 --> T31[Task6]
    M3 --> T32[Task7]
```


As described in the graph, the main part of Jupiter is the application. Each scraper should be added as a module in the application. This is done in the `main.py` where the execution of the framework begins.
```py
app = Application([
    ModuleA,
    ModuleB,
    ModuleC,
    ...
])
app.start()
```

Each module will have a list of tasks which will be run when the application starts or by the given schedule.

### Modules
Each scraper should be defined as individual modules. They should be defined as the subclass of `Scraper`. 
The structure of each module class will be like:
```py
class ModuleName(Scraper):
    def __init__(self, telemetry_client: TelemetryClient):
        super().__init__(telemetry_client)

    def on_startup():
        pass

    @staticmethod
    def meta() -> ModuleMeta:
        return ModuleMeta(
            name="Module Name",
            description="brief on what's happening in this module",
            version="0.0.0",
            author="Name <email>",
            team=Team.OSINT
        )

    @staticmethod
    def tasks() -> List[ScrapeTask]:
        return [] 
```
Each module will have the following built-in.
- `client` - An httpx client which handles proxies and observability under the hood.
- `db` - A MongoDB database instance which could be used for performing all the DB operations for the module

Each module should implement the following methods. 
    - `meta`
    - `tasks`
If you want to load some state on the startup of the scraper, you can use this.
    - `on_startup`
#### `meta`
This defines the metadata about the modules. 
Example: 
```py
from jupiter.lib.types import ModuleMeta, Team, TaskRepeatOptions
...
@staticmethod
def meta() -> ModuleMeta:
    return ModuleMeta(
        name="HoHoHo Scraper",
        description="brief on what's happening in this module",
        version="0.0.0",
        author="Name of the person responsible for the module",
        team=Team.OSINT
    )
```
> Note that `meta` should be defined with the decorator `@staticmethod`. 

`name` should be unique among the modules added to the application

There will not be any task duplication. Only one instance of a task will be run at a time. If the task did not finish before its next repeat, that will be skipped and the currently executing task will be continued.

#### `tasks`
This should return the list of tasks in this module.
```py
from typing import AsyncGenerator
from jupiter.lib.types import (
    RateLimitInterval,
    RateLimitOptions,
    ScrapeTask,
)


class HoHoHoScraper(Scraper):
    ...
    async def username_generator()-> AsyncGenerator[str, None]:
        yield "user1"
        yield "user2"
        yield "user3"

    async def username_processor(username: str) -> None:
        print(f"Processing username {username}")
    
    async 
    @staticmethod
    def tasks() -> List[ScrapeTask]:
        return [
            ScrapeTask(
                name="task-name",
                input_generator="username_generator",
                processor="username_processor",
                repeat_options=TaskRepeatOptions(
                    cron_pattern="* * * * *", start_immidiately=False
                ),
                rate_limiting_options=RateLimitOptions(
                    max_tasks=10,
                    interval=RateLimitInterval.MINUTE,
                ),
            ),
            ...
        ]
```
As mentioned in the example, each item in the list should be a `ScrapeTask` object. Properties of this class are:
- `name` : A unique name for the task. This will be used to identify the task in the backend.
- `input_generator` : The name of a `AsyncGenerator` method defined in the module. Each of the value yielded by this generator will be fed to the consumer function.
- `processor` : The name of an async method defined in the module. Each of the input yielded by the input generator will be processed by this.
- `repeat_options` : This provides with option to configure the task to be executed repeatedly. When to execute the task should be expressed in the `cron_pattern` field using the UNIX Cron format (The one with 5 fields). If `start_immediately=True`, the task will be run as soon as the background workers are ready to process. Otherwise, it will only be run in the next estimated time (i.e.: if `* * * * *` is given, the task will start running at the next minute onwards if `start_immediately` is `False`)
- `rate_limit_options` : Task execution can be rate limited by minute, hour and by second. The maximum no of tasks that can be executed within the time limit should be mentioned in `max_tasks` and the time limit in `interval`. 
  
Other values supported by the `interval` field are from the following Enum.
```
class RateLimitInterval(Enum):
    SECOND = "second"
    MINUTE = "minute"
    HOUR = "hour"
```

Rate limiting applies to the input production as well as the task execution. If the rate limiting is 10 tasks per minute, then only 10 inputs will be produced in each minute. Their processing will be distributed evenly in the time limit. That is the first input will be processed in the start of the minute, the next at 6th second, the next at 12th second, etc. and the 10th one at the 54th second. This is introduced as a safety measure to not fill up the Redis (or whichever broker is being used) with the generated input messages.

> Note that `tasks` should be defined with the decorator `@staticmethod`. 

Each task will be run by separate workers and consumer functions in the task will be run concurrently with the inputs provided by the generator.

#### `on_startup`
This method is invoked when the scraper is about to start. You could use this method to load some sort of state your scraper needs.
```py
class ModuleName(Scraper):
    ...
    def on_startup(self):
        self.my_collection.create_many({
            ...
        })
    ...
```

>If for some reason you need to disable a specific scraper, you could always use the `DISABLED_MODULES` environment variable. It expects the name of the modules to be disabled as in their `meta`, as a comma separated string. E.g: `DISABLED_MODULES=5Degrees,Sanctions` 

### Database
All the DB operations should be performed using the `db` property available in the scope of the module. The DB will be non-relational, as we are using MongoDB.
Creation of collections should be done in the constructor of the module class. As many collections can be created as needed. 

The DB used for each module will be in the name of itself. For example, DB of the module with name "HoHoHo Scraper" will be `ho_ho_ho_scraper` and all the collections created in that module will be inside that DB.

Example:
```py
from jupiter.lib.module import ScraperModule
...
class HoHoHoScraper(ScraperModule):
    def __init__(self) -> None:
        super().__init__()
        self.collection = self.db["tweets"]
```
For more info on how to do the CRUD operation using this, it is recommended to follow the documentation of the `pymongo` library available [here](https://pymongo.readthedocs.io/en/stable/).


### Observability
For each module, there exist a telemetry client built-in, which helps to create various metrics about the flow of tasks. It can be accessed like:
```py
class HoHoHoScraper(Scraper):
    ...
    async def username_generator(self)-> AsyncGenerator[str, None]:
        yield "user1"
        yield "user2"
        yield "user3"

    async def username_processor(self, username: str) -> None:
        print(f"Processing username {username}")
        self.telemetry_client.data_ingestion_counter.labels(self.meta().name, "username_fetching", "profiles").inc()
  ... 
```

In the above example, on processing each username, the `data_ingestion_counter` will be incremented. This helps keeping track of the data collected by each task.
The other counters are 
- success_counter
  ```py
        self.telemetry_client.success_counter.labels(self.meta().name, "username_fetching", "enrichProfile").inc()
  ```
- failure_counter
  ```py
        self.telemetry_client.failure_counter.labels(self.meta().name, "username_fetching", "getProfile").inc()
  ```
- warning_counter
  ```py
        self.telemetry_client.warning_counter.labels(self.meta().name, "username_fetching", "getProfilePicture").inc()
  ```
For each of the above counters, the first 2 labels would be the module name and the task name respectively. The third label, if present would be a custom label you can use.