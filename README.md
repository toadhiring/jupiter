# Jupiter

A python framework for building and maintaining scrapers

### Prerequisite
- [Poetry](https://python-poetry.org/docs/) for python dependency management
- [Docker](https://www.docker.com/) for running different services required for Jupiter, like the DB, Redis, etc

### Cloning the Repo
```sh
git clone https://gitlab.com/toadhiring/jupiter.git
```


- [Getting Started](./docs/getting-started.md)
- [Configuration](./docs/configuration.md)
- [Monitoring](./docs/monitoring.md)
- [Module Testing](./docs/testing.md)


### Collaboration Guide
You are supposed to share your commits to our mailing list. 

#### The setup
You need to make some changes to your `.git/config` of the cloned repo of Jupiter.
```bash
[sendemail]
	smtpserver = mail.example.org  # This should be the SMTP host of your email provider
	smtpuser = you@example.org  # Your email address at the SMTP host
	smtpencryption = ssl
	smtpserverport = 465
	to = hiring@toadmanagementconsulting.com
```

Once you have made your commits locally, you should send and email with all the patches containing your commits.

Say if your have 3 commits to send, then
```bash
git send-email HEAD~3
```
