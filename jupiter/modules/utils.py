import base58
import bech32
import hashlib
from enum import IntEnum


def validate_btc(address: str) -> bool:
    """ScamSearch exclusively does BTC reports. So we make sure all addresses are BTC"""
    ## FIX THIS: couldn't find any python package for this. this seems to work when tested for a set of 20k addresses
    try:
        if address[0] == "1" or address[0] == "3":
            try:
                bitcoin_bytes = base58.b58decode(address)
                return (
                    bitcoin_bytes[-4:]
                    == hashlib.sha256(
                        hashlib.sha256(bitcoin_bytes[:-4]).digest()
                    ).digest()[:4]
                )
            except Exception:
                return False
        elif address[:3] == "bc1":
            try:
                _, data = bech32.decode("bc", address)
                return data is not None
            except Exception:
                return False
        else:
            return False
    except:
        return False


class MongoErrCodes(IntEnum):
    DUPLICATE_ERR = 11000
