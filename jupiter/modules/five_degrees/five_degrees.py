from datetime import datetime
from typing import AsyncGenerator, List
from loguru import logger

from jupiter.lib.metrics import TelemetryClient

from jupiter.lib.module import Scraper
from jupiter.lib.types import (
    Team,
    ModuleMeta,
    ScrapeTask,
    TaskRepeatOptions,
    RateLimitInterval,
    RateLimitOptions,
)


class FiveDegrees(Scraper):
    def __init__(self, telemetry_client: TelemetryClient) -> None:
        super().__init__(telemetry_client)
        self.address_collection = self.db["addresses"]
        self.profile_collection = self.db["profiles"]

    @staticmethod
    def meta() -> ModuleMeta:
        return ModuleMeta(
            name="5Degrees",
            description="5 degress",
            author="Author Name <author@mail.com>",
            version="0.1",
            team=Team.OSINT,
        )

    def on_startup(self) -> None:
        self.telemetry_client.warning_counter.labels(
            self.name, "on_startup", "indicator"
        ).inc()

    async def page_num_generator(self) -> AsyncGenerator[int, None]:
        for page_num in range(10):
            yield page_num

    async def scrape_users(self, page_num: int) -> None:
        response = await self.client.get(
            f"https://openapi.5degrees.io/trending?pageNum={page_num}&address"
        )
        response_items = response.json()["data"]["list"]
        for user in response_items:
            if user["user"]:
                result = self.address_collection.update_one(
                    {"address": user["user"]},
                    {"$set": {"address": user["user"]}},
                    upsert=True,
                )
                self.telemetry_client.data_ingestion_counter.labels(
                    self.meta().name, "address_fetching", "wallet_address"
                ).inc(result.modified_count)

    async def address_generator(self) -> AsyncGenerator[List[str], None]:
        batch_size = 20
        results = self.address_collection.find({}, limit=batch_size, sort=[("_id", 1)])
        result_size = len(list(results.clone()))
        iteration = 1
        while result_size != 0:
            addresses = list(map(lambda result: result["address"], results))  # type: ignore
            logger.info(f"Yielding {len(addresses)} addresses")
            yield addresses
            results = self.address_collection.find(
                {}, limit=batch_size, sort=[("_id", 1)], skip=iteration * result_size
            )
            result_size = len(list(results.clone()))
            iteration += 1
        logger.info("Finished generating addresses")

    async def scrape_address(self, address: str) -> None:
        headers = {
            "authority": "openapi.5degrees.io",
            "accept": "application/json, text/plain, */*",
            "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,el;q=0.7",
            "if-none-match": 'W/"a7a7-/lDyFbXZi1F0OLLrBNS9GzThdVo"',
            "origin": "https://fans3.5degrees.io",
            "referer": "https://fans3.5degrees.io/",
            "sec-ch-ua": '"Google Chrome";v="107", "Chromium";v="107", "Not=A?Brand";v="24"',
            "sec-ch-ua-mobile": "?1",
            "sec-ch-ua-platform": '"Android"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "user-agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) "
            + "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Mobile Safari/537.36",
        }
        params = {
            "address": f"{address}",
            "chainId": "56",
        }
        response = await self.client.get(
            "https://openapi.5degrees.io/follow", params=params, headers=headers
        )
        for user in response.json()["data"]["followers"]:
            result = self.profile_collection.update_one(
                {"address": user["address"]},
                {
                    "$set": {
                        "address": user["address"],
                        "info_in_base64": user["info"],
                        "fetch_date": str(datetime.now()),
                    }
                },
                upsert=True,
            )
            self.telemetry_client.data_ingestion_counter.labels(
                self.meta().name, "profile_fetching", "profiles"
            ).inc(result.modified_count)

        for user in response.json()["data"]["followings"]:
            result = self.profile_collection.update_one(
                {"address": user["address"]},
                {
                    "$set": {
                        "address": user["address"],
                        "info_in_base64": user["info"],
                        "fetch_date": str(datetime.now()),
                    }
                },
                upsert=True,
            )
            self.telemetry_client.data_ingestion_counter.labels(
                self.meta().name, "profile_fetching", "profiles"
            ).inc(result.modified_count)
        logger.info(f"Finished scraping {address}")

    async def scrape_address_info(self, addresses: List[str]) -> None:
        for address in addresses:
            await self.scrape_address(address)

    @staticmethod
    def tasks() -> List[ScrapeTask]:
        return [
            ScrapeTask(
                name="address_fetching",
                input_generator="page_num_generator",
                processor="scrape_users",
                rate_limiting_options=RateLimitOptions(
                    max_tasks=30,
                    interval=RateLimitInterval.MINUTE,
                ),
                repeat_options=TaskRepeatOptions(
                    cron_pattern="0 0 * * 0", start_immediately=True  # Every week
                ),
            ),
            ScrapeTask(
                name="profile_fetching",
                input_generator="address_generator",
                processor="scrape_address_info",
                rate_limiting_options=RateLimitOptions(
                    max_tasks=30,
                    interval=RateLimitInterval.MINUTE,
                ),
                repeat_options=TaskRepeatOptions(
                    cron_pattern="0 0 * * 0", start_immediately=True  # Every week
                ),
            ),
        ]
