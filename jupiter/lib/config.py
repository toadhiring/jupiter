import os
from dataclasses import dataclass
from typing import List, Optional

from dotenv import load_dotenv

from jupiter.lib.helpers import to_snake_case


class EnvVarNotFound(Exception):
    def __init__(self, name: str) -> None:
        super().__init__(f"Environment variable {name} is required but not found")


@dataclass
class DbParams:
    host: str
    port: int
    user: str
    password: str

    def connection_url(self) -> str:
        if production_mode:
            return f"mongodb+srv://{self.user}:{self.password}@{self.host}"
        return f"mongodb://{self.user}:{self.password}@{self.host}:{self.port}"


@dataclass
class RedisParams:
    host: str
    port: int
    user: str
    password: str
    db: int

    def connection_url(self) -> str:
        return f"redis://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"


@dataclass
class CeleryConfig:
    queue_name: str
    broker_url: str
    backend_url: str
    concurrency: int


@dataclass
class Config:
    production_mode: bool
    telemetry_port: int
    celery: CeleryConfig
    db_params: DbParams
    redis_params: RedisParams
    disabled_modules: List[str]


def env(path: str, default: Optional[str]) -> Optional[str]:
    return os.environ.get(path, default)


def required_env(path: str, default: Optional[str] = None) -> str:
    value = env(path, default)
    if value is None:
        raise EnvVarNotFound(path)
    return value


def get_disabled_modules(disabled_modules: Optional[str]) -> List[str]:
    if disabled_modules is None:
        disabled_modules = ""
    return list(
        filter(
            lambda name: len(name) > 0,
            map(lambda name: to_snake_case(name.strip()), disabled_modules.split(",")),
        )
    )


def load_all_envs() -> None:
    paths: List[str] = ["tests/test.env"] if os.environ.get("APP_ENV") == "test" else []
    for dirpath, _, filenames in os.walk("jupiter/modules"):
        env_paths: List[str] = list(
            map(
                lambda filename: f"{dirpath}/{filename}",  # pylint: disable=cell-var-from-loop
                filter(lambda filename: filename == "secrets.env", filenames),
            )
        )
        paths.extend(env_paths)
    for path in paths:
        load_dotenv(path)


load_all_envs()

production_mode = required_env("PRODUCTION", "False") == "True"

db_params = DbParams(
    host=required_env("DB_HOST"),
    port=int(required_env("DB_PORT", "27017")),
    user=required_env("DB_USER"),
    password=required_env("DB_PASSWORD"),
)
redis_params = RedisParams(
    host=required_env("REDIS_HOST"),
    port=int(required_env("REDIS_PORT", "6379")),
    user=required_env("REDIS_USER"),
    password=required_env("REDIS_PASSWORD"),
    db=int(required_env("REDIS_DB")),
)

config = Config(
    production_mode=production_mode,
    telemetry_port=int(required_env("TELEMETRY_PORT", "3333")),
    celery=CeleryConfig(
        queue_name=required_env("CELERY_QUEUE_NAME", "jupiter"),
        broker_url=required_env("CELERY_BROKER_URL"),
        backend_url=required_env("CELERY_RESULT_BACKEND_URL"),
        concurrency=int(required_env("CELERY_WORKER_CONCURRENCY", "10")),
    ),
    db_params=db_params,
    redis_params=redis_params,
    disabled_modules=get_disabled_modules(
        disabled_modules=env("DISABLED_MODULES", None)
    ),
)
