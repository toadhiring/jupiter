import json
import subprocess
from typing import Any, AsyncGenerator, Callable, Coroutine, List, Optional

from jupiter.lib.types import IndexingOptions, TransformStatus


def get_command(opts: IndexingOptions) -> List[str]:
    command = [
        "spec",
        "-y",
        opts.yaml_path,
        "-n",
        opts.namespace,
        "-b",
        opts.indexing_branch_name,
        "-k",
        opts.indexing_branch_key,
        "-t",
        str(opts.telemetry_port) if opts.telemetry_port is not None else "5555",
        "-s",
    ]
    if opts.concurrent_requests is not None:
        command.extend(("-i", str(opts.concurrent_requests)))

    return command


def get_transform_status(exit_code: int) -> TransformStatus:
    status = TransformStatus.UNKNOWN_FAILURE
    max_status = max(member.value for member in TransformStatus)
    if exit_code <= max_status:
        status = TransformStatus(exit_code)
    return status


async def transform(
    input_generator: Callable[[], AsyncGenerator[Any, Any]],
    options: IndexingOptions,
    on_completed: Callable[
        [Any, TransformStatus, Optional[str]], Coroutine[Any, Any, None]
    ],
) -> None:
    command = get_command(options)
    async for value in input_generator():
        with subprocess.Popen(
            command,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        ) as process:
            _, stderr = process.communicate(input=json.dumps(value).encode())
            return_code = process.wait()
            status = get_transform_status(return_code)
            await on_completed(value, status, stderr.decode().strip())
