from re import sub
from typing import TypeVar

from croniter import croniter
from typing import TypeVar


def to_snake_case(text: str) -> str:
    return "_".join(
        sub(
            r"(\s|_|-)+",
            " ",
            sub(
                r"[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+",
                lambda mo: " " + str(mo.group(0).lower()),
                text,
            ),
        ).split()
    )


def is_valid_cron(cron_string: str) -> bool:
    if len(cron_string.split(" ")) > 5:
        return False
    return croniter.is_valid(cron_string)


Output = TypeVar("Output")
