from loguru import logger
from pymongo import MongoClient
from pymongo.collection import Collection
from pymongo.database import Database

from jupiter.lib.config import config
from jupiter.lib.helpers import to_snake_case
from jupiter.lib.types import InternalCollections

db_client: MongoClient
INTERNAL_DB_NAME = "internal"


class DBConnectionError(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__("Error creating DB connection", args)


def connect_db() -> bool:
    global db_client  # pylint:disable=invalid-name,global-statement
    try:
        db_client = MongoClient(config.db_params.connection_url())
        return True
    except Exception as error:  # pylint:disable=broad-exception-caught
        logger.error("Error creating DB connection", error)
        raise error


def get_module_db(module_name: str) -> Database:
    db_name = to_snake_case(module_name)
    return db_client[db_name]


def get_internal_collection(collection_name: InternalCollections) -> Collection:
    return db_client[INTERNAL_DB_NAME][collection_name.value]
