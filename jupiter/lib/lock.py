from time import sleep

from loguru import logger
from jupiter.lib.redis_client import redis_client

LOCK_SUFFIX = "lock"


def get_lock_name(name: str) -> str:
    return f"{name}::{LOCK_SUFFIX}"


def is_locked(name: str) -> bool:
    lock_name = get_lock_name(name)
    return redis_client.exists(lock_name) == 1


def acquire_lock(name: str) -> bool:
    if is_locked(name):
        return False
    lock_name = get_lock_name(name)
    logger.info(f"Locking {name}")
    return redis_client.set(lock_name, 1) is True


def release_lock(name: str) -> bool:
    logger.info(f"Releasing lock for {name}")
    lock_name = get_lock_name(name)
    return redis_client.delete(lock_name) == 1


def release_all_locks() -> bool:
    lock_keys = redis_client.keys(f"*::{LOCK_SUFFIX}")
    keys_count = len(lock_keys)
    if keys_count == 0:
        return True
    logger.info(f"Releasing {keys_count} keys")
    return redis_client.delete(*lock_keys) == keys_count


def wait_until_release(name: str) -> None:
    while is_locked(name):
        sleep(0.5)
