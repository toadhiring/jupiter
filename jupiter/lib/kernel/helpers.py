from typing import List
from jupiter.lib.helpers import to_snake_case
from jupiter.lib.types import RateLimitInterval, RateLimitOptions

SCOPE_SEPARATOR = "::"
SCHEDULER_TASK_IDENTIFIER = "scheduler"
TRAFFIC_MANAGER_IDENTIFIER = "traffic"


def create_scoped_name(*args: List[str]) -> str:
    return SCOPE_SEPARATOR.join(*args)


def get_task_name(module_name: str, task_name: str) -> str:
    return create_scoped_name([to_snake_case(module_name), to_snake_case(task_name)])


def get_rate_limit_pattern(rate_limit_options: RateLimitOptions) -> str:
    match rate_limit_options.interval:
        case RateLimitInterval.HOUR:
            interval = "h"
        case RateLimitInterval.MINUTE:
            interval = "m"
        case RateLimitInterval.SECOND:
            interval = "s"
    return f"{rate_limit_options.max_tasks}/{interval}"


def get_scheduler_task_name(module_name: str, task_name: str) -> str:
    return create_scoped_name(
        [get_task_name(module_name, task_name), SCHEDULER_TASK_IDENTIFIER]
    )


def is_scheduler_task_name(text: str) -> bool:
    return text.split(SCOPE_SEPARATOR)[-1] == SCHEDULER_TASK_IDENTIFIER


def get_traffic_manager_name(module_name: str, task_name: str) -> str:
    return create_scoped_name(
        [get_task_name(module_name, task_name), TRAFFIC_MANAGER_IDENTIFIER]
    )
