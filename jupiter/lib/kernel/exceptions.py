class InvalidCronPatternForTask(Exception):
    def __init__(self, module_name: str, task_name: str, cron_pattern: str) -> None:
        super().__init__(
            f"Invalid cron pattern given for task {task_name} in module {module_name}."
            + f"Given pattern: {cron_pattern}"
        )


class NoMethodFoundInModule(Exception):
    def __init__(self, module_name: str, member_name: str) -> None:
        super().__init__(f"Could not find method {member_name} in module {module_name}")


class TrafficLockAcquisitionFailed(Exception):
    def __init__(self, task_name: str) -> None:
        super().__init__(f"Traffic lock acquisition failed for task {task_name}")
