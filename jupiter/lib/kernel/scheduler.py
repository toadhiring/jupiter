import asyncio
from typing import AsyncGenerator, Callable, List, Type, TypeVar
from celery import Task, chord
from celery.canvas import Signature
from loguru import logger
from jupiter.lib.kernel.exceptions import (
    NoMethodFoundInModule,
    TrafficLockAcquisitionFailed,
)

from jupiter.lib.kernel.helpers import (
    get_scheduler_task_name,
    get_task_name,
    get_traffic_manager_name,
)
from jupiter.lib.lock import acquire_lock, is_locked, release_lock, wait_until_release
from jupiter.lib.metrics import TelemetryClient
from jupiter.lib.types import ScrapeTask
from jupiter.lib.module import Scraper

GeneratorYield = TypeVar("GeneratorYield")


async def run_tasks(
    celery_task: Task,  # type: ignore
    input_generator: Callable[[], AsyncGenerator[GeneratorYield, None]],  # type: ignore
) -> None:
    # Creates tasks without any rate limiting
    async for value in input_generator():
        celery_task.apply_async((value,))


def run_batched_tasks(
    module_name: str,
    task_name: str,
    task_list: List[Signature],  # type: ignore
    traffic_manager_task: Task,  # type: ignore
) -> None:
    traffic_lock_name = get_traffic_manager_name(module_name, task_name)
    status = acquire_lock(traffic_lock_name)
    if not status:
        raise TrafficLockAcquisitionFailed(task_name)
    chord(task_list)(traffic_manager_task.signature((module_name, task_name)))  # type: ignore
    wait_until_release(traffic_lock_name)


async def run_scheduler_task(
    module: Type[Scraper],
    task_definition: ScrapeTask,
    celery_task: Task,  # type: ignore
    traffic_manager_task: Task,  # type: ignore
    telemetry_client: TelemetryClient,
) -> None:
    module_object = module(telemetry_client)
    module_name = module.meta().name
    module_object.telemetry_client.scheduler_execution_counter.labels(
        module_name, task_definition.name
    ).inc()
    input_generator = getattr(module_object, task_definition.input_generator, None)
    if input_generator is None:
        module_object.telemetry_client.task_exception_counter.labels(
            "exception_name", NoMethodFoundInModule.__name__
        ).inc()
        raise NoMethodFoundInModule(
            module_name=module_name, member_name=task_definition.input_generator
        )
    task_name = get_task_name(module_name, task_definition.name)
    if task_definition.rate_limiting_options is None:
        await run_tasks(celery_task, input_generator)
        return

    max_tasks = task_definition.rate_limiting_options.max_tasks
    task_list = []

    async for value in input_generator():
        task_list.append(celery_task.signature((value,)))  # type: ignore
        if len(task_list) >= max_tasks:
            run_batched_tasks(module_name, task_name, task_list, traffic_manager_task)
            task_list = []
    if len(task_list) > 0:
        run_batched_tasks(module_name, task_name, task_list, traffic_manager_task)


def get_scheduler_task_fn(
    module: Type[Scraper],
    task_definition: ScrapeTask,
    celery_task: Task,  # type: ignore
    traffic_manager_task: Task,  # type: ignore
    telemetry_client: TelemetryClient,
) -> Callable[[], None]:
    def scheduler_task_fn() -> None:
        task_name = get_scheduler_task_name(
            module_name=module.meta().name, task_name=task_definition.name
        )
        if is_locked(task_name):
            logger.info(f"Already running task found for {task_name}")
            return
        if not acquire_lock(task_name):
            logger.info(
                f"Could not acquire lock for task {task_name}. Skipping execution"
            )
            return
        loop = asyncio.get_event_loop()
        loop.run_until_complete(
            run_scheduler_task(
                module,
                task_definition,
                celery_task,
                traffic_manager_task,
                telemetry_client,
            )
        )
        release_lock(task_name)

    return scheduler_task_fn
