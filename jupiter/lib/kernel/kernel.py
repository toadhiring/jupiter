import asyncio
from dataclasses import dataclass
from typing import Any, Coroutine, Dict, List, Optional, Type, Callable, Union
from _collections_abc import dict_items
from celery import Celery, Task
from celery.schedules import crontab
from celery.signals import worker_ready
from loguru import logger
from jupiter.lib.config import config
from jupiter.lib.helpers import is_valid_cron
from jupiter.lib.kernel.exceptions import InvalidCronPatternForTask
from jupiter.lib.kernel.helpers import (
    get_rate_limit_pattern,
    get_scheduler_task_name,
    get_task_name,
    get_traffic_manager_name,
)
from jupiter.lib.lock import release_all_locks, release_lock
from jupiter.lib.kernel.scheduler import get_scheduler_task_fn  # type: ignore
from jupiter.lib.metrics import TelemetryClient
from jupiter.lib.module import Scraper
from jupiter.lib.transform import transform
from jupiter.lib.types import ScrapeTask, TaskRepeatOptions, TransformStatus


INDEXING_TASK_NAME = "indexing"
INDEXING_INPUT_GENERATOR_NAME = "indexing_input_generator"


@dataclass
class TaskRegistryItem:
    task: Task  # type: ignore
    start_immediately: bool
    scheduler_task: Optional[Task] = None  # type: ignore


class TaskRegistry:
    __registry: Dict[str, TaskRegistryItem] = {}

    def add_task(
        self,
        task_name: str,
        task: Task,  # type: ignore
        start_immediately: bool = False,
    ) -> None:
        self.__registry[task_name] = TaskRegistryItem(
            task=task, start_immediately=start_immediately
        )

    def add_scheduler(self, task_name: str, scheduler_task: Task):  # type: ignore
        self.__registry[task_name].scheduler_task = scheduler_task

    def get(self, task_name: str) -> TaskRegistryItem:
        item = self.__registry.get(task_name)
        if item is None:
            raise ValueError("Task name not found in Task registry")
        return item

    def get_task(self, task_name: str) -> Task:  # type: ignore
        return self.get(task_name).task  # type: ignore

    def get_scheduler(self, task_name: str) -> Task:  # type: ignore
        return self.get(task_name).scheduler_task  # type: ignore

    def items(self) -> dict_items:  # type: ignore
        return self.__registry.items()


def get_task_fn(
    module: Type[Scraper],
    task_definition: ScrapeTask,
    telemetry_client: TelemetryClient,
) -> Callable[[object], None]:
    def task_fn(value: object) -> None:  # type: ignore
        module_object = module(telemetry_client)
        processor = getattr(module_object, task_definition.processor)
        loop = asyncio.get_event_loop()
        telemetry_client.task_execution_counter.labels(
            module.meta().name, task_definition.name
        ).inc()
        loop.run_until_complete(processor(value))
        telemetry_client.task_completion_counter.labels(
            module.meta().name, task_definition.name
        ).inc()

    return task_fn  # type: ignore


def get_indexing_task_fn(
    module: Type[Scraper], telemetry_client: TelemetryClient
) -> Callable[[], None]:
    def indexing_task_fn() -> None:  # type: ignore
        module_object = module(telemetry_client)
        loop = asyncio.get_event_loop()
        indexing_input_generator = getattr(module_object, INDEXING_INPUT_GENERATOR_NAME)
        on_completed: Callable[
            [Any, TransformStatus, Optional[str]], Coroutine[Any, Any, None]
        ] | None = getattr(
            module_object, module.indexing_options().on_completed, None  # type: ignore
        )
        if on_completed is None:
            raise AttributeError(
                f"{module.indexing_options().on_completed} method not found "  # type: ignore
                + f" in {module_object.meta().name} module"
            )

        loop.run_until_complete(
            transform(
                indexing_input_generator,
                module.indexing_options(),  # type: ignore
                on_completed,
            )
        )
        telemetry_client.task_completion_counter.labels(
            module.meta().name, INDEXING_TASK_NAME
        ).inc()

    return indexing_task_fn


def get_traffic_manager_task_fn() -> Callable[[List, str, str], None]:  # type: ignore
    def traffic_manager_task_fn(_, module_name: str, task_name: str) -> None:  # type: ignore
        lock_name = get_traffic_manager_name(module_name, task_name)
        release_lock(lock_name)

    return traffic_manager_task_fn  # type: ignore


class Kernel:
    def __init__(self, telemetry_client: TelemetryClient) -> None:
        self.celery = Celery(
            "jupiter",
            broker=config.celery.broker_url,
            backend=config.celery.backend_url,  # To be handled in #PPLD-48
        )
        self.traffic_manager_task = self.celery.task(name="traffic_manager")(  # type: ignore
            get_traffic_manager_task_fn()  # type: ignore
        )
        self.task_registry = TaskRegistry()
        self.telemetry_client = telemetry_client

    def register_task(self, module: Type[Scraper], task_definition: ScrapeTask) -> None:
        # registering the actual task
        task_name = get_task_name(
            module_name=module.meta().name, task_name=task_definition.name
        )
        task_fn = get_task_fn(module, task_definition, self.telemetry_client)
        rate_limit = None
        if task_definition.rate_limiting_options is not None:
            rate_limit = get_rate_limit_pattern(task_definition.rate_limiting_options)
        start_immediately = False
        if task_definition.repeat_options is not None:
            start_immediately = (
                task_definition.repeat_options.start_immediately or False
            )
        task = self.celery.task(name=task_name, rate_limit=rate_limit)(  # type: ignore
            task_fn
        )
        self.task_registry.add_task(task_name, task, start_immediately)  # type: ignore

    def register_indexing_task(self, module: Type[Scraper]) -> None:
        if module.indexing_options() is None:
            raise AttributeError(
                "Module with indexing enabled must have indexing_options defined."
            )
        task_name = get_task_name(module.meta().name, INDEXING_TASK_NAME)
        indexing_task_fn = get_indexing_task_fn(module, self.telemetry_client)
        start_immediately = (
            module.indexing_options().repeat_options.start_immediately or False  # type: ignore
        )
        task = self.celery.task(name=task_name)(indexing_task_fn)  # type: ignore
        self.task_registry.add_task(task_name, task, start_immediately)  # type: ignore
        repeat_options = module.indexing_options().repeat_options  # type: ignore
        if not is_valid_cron(repeat_options.cron_pattern):
            raise InvalidCronPatternForTask(
                module_name=module.meta().name,
                task_name=task_name,
                cron_pattern=repeat_options.cron_pattern,
            )
        (
            minute,
            hour,
            day_of_month,
            month,
            day_of_week,
        ) = repeat_options.cron_pattern.split(" ")

        beat_schedule = {  # type: ignore
            task_name: {
                "task": task_name,
                "schedule": crontab(
                    minute=minute,
                    hour=hour,
                    day_of_month=day_of_month,
                    month_of_year=month,
                    day_of_week=day_of_week,
                ),
                "args": [],
            }
        }
        self.celery.conf.beat_schedule.update(beat_schedule)

    def __configure_schedule(
        self,
        module: Type[Scraper],
        task_name: str,
        repeat_options: Optional[TaskRepeatOptions],
    ) -> None:
        if repeat_options is None:
            return
        if not is_valid_cron(repeat_options.cron_pattern):
            raise InvalidCronPatternForTask(
                module_name=module.meta().name,
                task_name=task_name,
                cron_pattern=repeat_options.cron_pattern,
            )
        (
            minute,
            hour,
            day_of_month,
            month,
            day_of_week,
        ) = repeat_options.cron_pattern.split(" ")

        scheduler_task_name = get_scheduler_task_name(module.meta().name, task_name)
        beat_schedule = {  # type: ignore
            scheduler_task_name: {
                "task": scheduler_task_name,
                "schedule": crontab(
                    minute=minute,
                    hour=hour,
                    day_of_month=day_of_month,
                    month_of_year=month,
                    day_of_week=day_of_week,
                ),
                "args": [],
            }
        }
        self.celery.conf.beat_schedule.update(beat_schedule)

    def register_scheduler_task(
        self, module: Type[Scraper], task_definition: ScrapeTask
    ) -> None:
        # registering a scheduler for the actual task.
        # This will be run by the given schedule and triggering the actual task
        actual_task_name = get_task_name(
            module_name=module.meta().name, task_name=task_definition.name
        )
        actual_task = self.task_registry.get_task(actual_task_name)  # type: ignore

        scheduler_task_name = get_scheduler_task_name(
            module.meta().name, task_definition.name
        )
        scheduler_task_fn = get_scheduler_task_fn(
            module,
            task_definition,
            actual_task,
            self.traffic_manager_task,  # type: ignore
            self.telemetry_client,
        )

        scheduler_task = self.celery.task(name=scheduler_task_name)(  # type: ignore
            scheduler_task_fn
        )
        self.task_registry.add_scheduler(actual_task_name, scheduler_task)  # type: ignore
        self.__configure_schedule(
            module, task_definition.name, task_definition.repeat_options
        )

    def __start_all_schedulers(self) -> None:
        for _, registry_item in self.task_registry.items():  # type: ignore
            if registry_item.scheduler_task is not None and registry_item.start_immediately:  # type: ignore
                registry_item.scheduler_task.apply_async()  # type: ignore

    def __on_worker_ready(
        self,
        sender: Optional[str] = None,  # pylint: disable=unused-argument
        conf: Optional[  # pylint: disable=unused-argument
            Dict[str, Union[str, int]]
        ] = None,
        **_: Optional[Dict[str, Union[str, int]]],
    ) -> None:
        self.__start_all_schedulers()

    def start(self) -> None:
        if release_all_locks():
            logger.info("Released all existing locks")
        else:
            logger.warning("Could not complete lock release")
        worker_ready.connect()(self.__on_worker_ready)
        worker_params = ["worker", "--loglevel=INFO", "-B"]
        if config.production_mode:
            self.celery.conf.broker_transport_options = {
                "region": "us-east-2",
            }
            worker_params.extend(
                [
                    "-Ofair",
                    f"--concurrency={config.celery.concurrency}",
                ]
            )
        self.celery.worker_main(worker_params)
