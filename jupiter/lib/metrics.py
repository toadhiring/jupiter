from loguru import logger
from prometheus_client import (
    CollectorRegistry,
    Counter,
    multiprocess,
    start_http_server,
)

from jupiter.lib.config import config

METRICS_PREFIX = "jupiter"
TELEMETRY_REQUESTS_COUNTER = "requests_counter"


class TelemetryClient:  # pylint: disable=too-many-instance-attributes
    def __init__(self) -> None:
        self.registry = CollectorRegistry()
        multiprocess.MultiProcessCollector(self.registry)
        self.proxy_exception_counter = Counter(
            f"{METRICS_PREFIX}_proxy_exception_counter",
            "Keeping count of proxy exceptions in each request",
            ["exception_name"],
            registry=self.registry,
        )
        self.request_counter = Counter(
            f"{METRICS_PREFIX}_request_counter",
            "Keeping count of response statuses of each request made",
            ["module_name", "url", "status_code"],
            registry=self.registry,
        )
        self.task_execution_counter = Counter(
            f"{METRICS_PREFIX}_task_execution_counter",
            "Keeping count of task executions in each modules",
            ["module_name", "task_name"],
            registry=self.registry,
        )
        self.scheduler_execution_counter = Counter(
            f"{METRICS_PREFIX}_scheduler_execution_counter",
            "Keeping count of scheduler executions for each tasks",
            ["module_name", "task_name"],
            registry=self.registry,
        )
        self.task_exception_counter = Counter(
            f"{METRICS_PREFIX}_exception_counter",
            "Keeping count of exceptions occurred while execution of various tasks",
            ["exception_name"],
            registry=self.registry,
        )
        self.task_completion_counter = Counter(
            f"{METRICS_PREFIX}_task_completion_counter",
            "Keeping count of completed tasks",
            ["module_name", "task_name"],
            registry=self.registry,
        )
        self.success_counter = Counter(
            f"{METRICS_PREFIX}_success_counter",
            "Keeping count of something that was happened the desired way",
            ["module_name", "task_name", "label"],
            registry=self.registry,
        )
        self.failure_counter = Counter(
            f"{METRICS_PREFIX}_failure_counter",
            "Keeping count of something that failed to happened the desired way",
            ["module_name", "task_name", "label"],
            registry=self.registry,
        )
        self.warning_counter = Counter(
            f"{METRICS_PREFIX}_warning_counter",
            "Keeping count of warnings to be handled",
            ["module_name", "task_name", "warning_name"],
            registry=self.registry,
        )
        self.data_ingestion_counter = Counter(
            f"{METRICS_PREFIX}_data_ingestion_counter",
            "Keeping count of data ingested by the respective task",
            ["module_name", "task_name", "data_label"],
            registry=self.registry,
        )
        self.indexing_counter = Counter(
            f"{METRICS_PREFIX}_indexing_counter",
            "Keeping count of data indexed to the Core DB",
            ["module_name"],
            registry=self.registry,
        )

    def start_metrics_server(self) -> None:
        logger.info(f"Starting metrics server on port {config.telemetry_port}")
        start_http_server(config.telemetry_port, registry=self.registry)
