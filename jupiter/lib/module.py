from abc import ABC, abstractmethod
from typing import Any, Generator, List

from loguru import logger


from jupiter.lib import db
from jupiter.lib.metrics import TelemetryClient
from jupiter.lib.request import create_request_client
from jupiter.lib.types import ModuleMeta, ScrapeTask, IndexingOptions


class Scraper(ABC):
    def __init__(self, telemetry_client: TelemetryClient) -> None:
        super().__init__()
        self.client = create_request_client(self.name, telemetry_client)
        self.db = db.get_module_db(self.name)
        self.telemetry_client = telemetry_client

    @staticmethod
    @abstractmethod
    def meta() -> ModuleMeta:
        pass

    @property
    def name(self) -> str:
        return self.meta().name

    def on_startup(self) -> None:
        logger.info(f"Starting up module {self.name}")

    @staticmethod
    @abstractmethod
    def tasks() -> List[ScrapeTask]:
        pass

    @staticmethod
    def indexing_options() -> IndexingOptions | None:
        pass

    def indexing_input_generator(
        self,
    ) -> Generator[Any, Any, Any] | None:
        pass
