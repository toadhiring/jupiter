from typing import List
import random
from attrs import define, field, validators

from jupiter.lib.db import get_internal_collection
from jupiter.lib.types import InternalCollections
from jupiter.lib.redis_client import redis_client


class NoProxyAvailable(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


@define
class ProxyCreds:
    host: str = field(validator=[validators.min_len(8)])
    port: int = field(validator=[validators.gt(0)])
    username: str = field()
    password: str = field()


def cache_proxies(proxies: List[ProxyCreds]) -> None:
    redis_client.delete(InternalCollections.PROXY.value)
    redis_client.sadd(
        InternalCollections.PROXY.value,
        *list(
            map(
                lambda proxy: f"{proxy.username}:{proxy.password}@{proxy.host}:{proxy.port}",
                proxies,
            )
        ),
    )


def get_proxy_from_cache() -> ProxyCreds:
    proxy_str = redis_client.srandmember(InternalCollections.PROXY.value)
    if proxy_str is None:
        raise NoProxyAvailable()
    auth, host_port = str(proxy_str.decode("utf-8")).split("@")  # type:ignore
    return ProxyCreds(
        host=host_port.split(":")[0],
        port=int(host_port.split(":")[1]),
        username=auth.split(":")[0],
        password=auth.split(":")[1],
    )


def get_proxy(no_cache: bool = False) -> ProxyCreds:
    if not no_cache:
        try:
            return get_proxy_from_cache()
        except NoProxyAvailable:
            pass
    proxy_collection = get_internal_collection(InternalCollections.PROXY)
    all_proxies = proxy_collection.find({})
    proxy_list: List[ProxyCreds] = []
    for doc in all_proxies:
        proxy_list.append(
            ProxyCreds(
                host=doc.get("host", ""),
                port=doc.get("port", 0),
                username=doc.get("username", ""),
                password=doc.get("password", ""),
            )
        )
    if len(proxy_list) == 0:
        raise NoProxyAvailable()
    cache_proxies(proxy_list)
    return random.choice(proxy_list)
