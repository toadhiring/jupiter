from redis import Redis

from jupiter.lib.config import config

redis_client = Redis.from_url(config.redis_params.connection_url())
