import sys
from typing import List, Type

from loguru import logger


from jupiter.lib import db
from jupiter.lib.helpers import to_snake_case
from jupiter.lib.kernel.kernel import Kernel
from jupiter.lib.metrics import TelemetryClient
from jupiter.lib.module import Scraper
from jupiter.lib.config import config


class Application:
    def __init__(self, modules: List[Type[Scraper]]) -> None:
        db_status = db.connect_db()
        if not db_status:
            logger.critical("Exiting application due to DB connection issues")
            sys.exit(1)
        self._modules = modules
        self.telemetry_client = TelemetryClient()
        self.kernel = Kernel(self.telemetry_client)

    def __init_tasks(self, module: Type[Scraper]) -> None:
        for task in module.tasks():
            self.kernel.register_task(module, task)  # type: ignore
            self.kernel.register_scheduler_task(module, task)  # type: ignore

    def __init_indexing(self, module: Type[Scraper]) -> None:
        if not module.meta().indexing_enabled:
            return
        self.kernel.register_indexing_task(module)

    def __init_modules(self) -> None:
        for module in self._modules:
            if (
                "*" in config.disabled_modules
                or to_snake_case(module.meta().name) in config.disabled_modules
            ):
                logger.warning(
                    f"{module.meta().name} found in disabled modules. Skipping initialization"
                )
                continue
            self.__init_tasks(module)
            self.__init_indexing(module)
            module(self.telemetry_client).on_startup()

    def start(self) -> None:
        self.telemetry_client.start_metrics_server()
        self.__init_modules()
        self.kernel.start()
