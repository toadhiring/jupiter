from dataclasses import dataclass
from enum import Enum
from typing import Optional


class Team(Enum):
    OSINT = "osint"


class InternalCollections(Enum):
    PROXY = "proxies"


class RateLimitInterval(Enum):
    SECOND = "second"
    MINUTE = "minute"
    HOUR = "hour"


@dataclass
class TaskRepeatOptions:
    cron_pattern: str  # cron pattern should be in UNIX format rather than Quartz format
    start_immediately: bool


@dataclass
class RateLimitOptions:
    interval: RateLimitInterval
    max_tasks: int


class TransformStatus(Enum):
    INDEXING_SUCCESS = 0
    UNKNOWN_FAILURE = 1
    INDEXING_REQUEST_FAILURE = 2
    INDEXING_FAILURE = 3
    INVALID_INPUT_JSON = 4
    INVALID_INPUT_YAML = 5
    INVALID_NAMESPACE = 6
    INVALID_RESPONSE_BUFFER_SIZE = 7
    INVALID_CONCURRENT_REQUESTS = 8


@dataclass
class IndexingOptions:
    yaml_path: str
    namespace: str
    indexing_branch_name: str
    indexing_branch_key: str
    concurrent_requests: Optional[int]
    on_completed: str  # This should be the name of an async generator defined in the module
    repeat_options: TaskRepeatOptions
    telemetry_port: Optional[int] = None


@dataclass
class ModuleMeta:
    name: str
    description: str
    version: str
    author: str
    team: Team
    indexing_enabled: bool = False


@dataclass
class ScrapeTask:
    name: str
    input_generator: str  # This should be the name of an async generator defined in the module
    processor: str  # This should be the name of the async processor defined in the module
    repeat_options: Optional[TaskRepeatOptions] = None
    rate_limiting_options: Optional[RateLimitOptions] = None
