import base64

from typing import Callable, Coroutine, Any
from httpx import AsyncClient, Request, Response
from jupiter.lib.metrics import TelemetryClient

from jupiter.lib.proxy import NoProxyAvailable, get_proxy

RequestEventHook = Callable[[Request], Coroutine[Any, Any, None]]  # type: ignore
RequestEventHandler = Callable[[Request, TelemetryClient, str], Coroutine[Any, Any, None]]  # type: ignore
ResponseEventHook = Callable[[Response], Coroutine[Any, Any, None]]  # type: ignore
ResponseEventHandler = Callable[[Response, TelemetryClient, str], Coroutine[Any, Any, None]]  # type: ignore


def get_request_event_hook(
    handler: RequestEventHandler, telemetry_client: TelemetryClient, module_name: str
) -> RequestEventHook:
    async def event_hook(request: Request) -> None:
        await handler(request, telemetry_client, module_name)

    return event_hook


def get_response_event_hook(
    handler: ResponseEventHandler, telemetry_client: TelemetryClient, module_name: str
) -> ResponseEventHook:
    async def event_hook(response: Response) -> None:
        await handler(response, telemetry_client, module_name)

    return event_hook


async def add_proxy(request: Request, _: TelemetryClient, __: str) -> None:
    try:
        proxy = get_proxy()
        proxy_str = f"{proxy.username}:{proxy.password}@{proxy.host}:{proxy.port}"
        request.headers["Proxy"] = base64.b64encode(bytes(proxy_str, "utf-8")).hex()
    except NoProxyAvailable:
        pass


async def create_response_metrics(
    response: Response, telemetry_client: TelemetryClient, module_name: str
) -> None:
    telemetry_client.request_counter.labels(
        module_name, response.url, response.status_code
    ).inc()


def create_request_client(
    module_name: str, telemetry_client: TelemetryClient
) -> AsyncClient:
    client = AsyncClient()
    client.event_hooks["request"] = [
        get_request_event_hook(add_proxy, telemetry_client, module_name),
    ]
    client.event_hooks["response"] = [
        get_response_event_hook(create_response_metrics, telemetry_client, module_name)
    ]
    return client
