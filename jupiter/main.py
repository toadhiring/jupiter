from typing import List, Type
from jupiter.lib.application import Application
from jupiter.lib.module import Scraper


def run() -> None:
    modules: List[Type[Scraper]] = []
    app = Application(modules)
    app.start()
