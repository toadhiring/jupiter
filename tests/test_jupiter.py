import asyncio

from dotenv import load_dotenv

from jupiter import __version__
from jupiter.lib import db
from jupiter.lib.config import get_disabled_modules
from jupiter.lib.proxy import get_proxy

load_dotenv()


async def test_db_client() -> None:
    status = db.connect_db()
    if not status:
        print("Could not connect to the DB. Test failed")
        return
    database = db.get_module_db("test_db")
    collection = database["test_collection"]
    response = collection.insert_one({"key": 1})
    if not response.acknowledged:
        print("Could not write to DB. Test failed")
        return
    print("Test success")


async def test_proxy() -> None:
    db.connect_db()
    proxy = get_proxy()
    print(proxy)


def test_disabled_modules() -> None:
    env_string = "5Degrees,Sanctions"
    assert get_disabled_modules(env_string) == ["5_degrees", "sanctions"]
    env_string = " 5Degrees, Sanctions"
    assert get_disabled_modules(env_string) == ["5_degrees", "sanctions"]
    env_string = " "
    assert not get_disabled_modules(env_string)
    env_string = ","
    assert not get_disabled_modules(env_string)


def run() -> None:
    asyncio.run(test_proxy())
