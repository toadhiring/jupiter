from typing import Any, Type
from jupiter.lib.db import connect_db
from jupiter.lib.metrics import TelemetryClient

from jupiter.lib.module import Scraper
from jupiter.lib.types import ScrapeTask, TransformStatus
from jupiter.modules.five_degrees.five_degrees import FiveDegrees


async def test_task(module_object: Scraper, task: ScrapeTask):
    input_generator = getattr(module_object, task.input_generator, None)
    processor = getattr(module_object, task.processor, None)
    assert input_generator is not None
    assert processor is not None
    value = await anext(input_generator())
    await processor(value)


async def on_completed(_: Any, status: TransformStatus, __: str | None):
    assert status == TransformStatus.INDEXING_SUCCESS
    print(status)


async def test(module: Type[Scraper], telemetry_client: TelemetryClient):
    tasks = module.tasks()
    module_object = module(telemetry_client)
    for task in tasks:
        await test_task(module_object, task)


def run():
    import asyncio

    connect_db()
    asyncio.run(test(FiveDegrees, TelemetryClient()))
